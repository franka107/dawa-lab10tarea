const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
  nombre: {
    type: String,
    max: 100,
    required: [true, "El nombre del usuario es necesario"],
  },
  precio: {
    type: String,
    max: 150,
    unique: true,
    required: [true, "El precio del usuario es necesario"],
  },
  imagen: {
    type: String,
    require: [true, "la imagen del usuario es necesario"],
  },
});

module.exports = mongoose.model("Product", ProductSchema);
