const express = require("express");
const port = process.env.PORT || 3000;
const bodyParser = require("body-parser");
const conn = require("./connection");
const jwt = require("jsonwebtoken");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const app = express();
const bcrypt = require("bcrypt");
const { verificarToken } = require("./middleware/authentication");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(
  "732505487071-cufjg0cfiqb73d51ffgovdnap0cm6gv8.apps.googleusercontent.com"
);

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/node_modules/bootstrap/dist"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: false,
  })
);

app.set("view engine", "pug");
//----------usuario-------------------

const User = require("./models/user.model");

app.get("/login", (req, res) => {
  res.render("login");
});
app.post("/login", (req, res) => {
  let body = req.body;

  //let user = new User({
  //username: "frank",
  //email: "fcaryv@gmail.com",
  //password: bcrypt.hashSync("123456", 10),
  //});
  //user.save();

  User.findOne({ email: body.email }, (err, usuarioDB) => {
    if (err) {
      return res.send("algo anda mal");
    }

    if (!usuarioDB) {
      return res.send("Usuario o contraseña incorrecta");
    }

    if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
      return res.send("usuario o contraseña incorrectos");
    }

    let token = jwt.sign(
      {
        usuario: usuarioDB,
      },
      "seed-desarrollo",
      { expiresIn: 60 * 60 * 34 * 30 }
    );
    console.log(token);
    //req.session.token = token;
    res.cookie("token", token);
    res.redirect("/");
  });
});
//----------producto--------------------
const Product = require("./models/product.model");
app.get("/eliminar/:id", verificarToken, (req, res) => {
  let id = req.params.id;
  Product.findByIdAndRemove(id, (err) => {
    res.redirect("/productos");
  });
});

app.get("/editar/:id", verificarToken, (req, res) => {
  let id = req.params.id;
  Product.findById(id, (err, product) => {
    res.render("detalle", { product });
  });
});
app.get("/crear", verificarToken, (req, res) => {
  res.render("crear");
});
app.post("/completado/:id", verificarToken, (req, res) => {
  let id = req.params.id;
  let fields = {
    nombre: req.body.nombre,
    precio: req.body.precio,
    imagen: req.body.imagen,
  };

  Product.findByIdAndUpdate(req.params.id, { $set: fields }, (err, product) => {
    res.redirect(`/productos`);
  });
});
app.post("/creado", verificarToken, (req, res) => {
  let product = new Product({
    nombre: req.body.nombre,
    precio: req.body.precio,
    imagen: req.body.imagen,
  });
  product.save((err) => (err ? next(err) : res.redirect("/productos")));
});
//--------------------------------------
app.get("/", (req, res) => {
  res.render("home", { logged: req.cookies.token ? true : false });
});

app.get("/quienes-somos", (req, res) => {
  res.render("about-us");
});

app.get("/contactanos", (req, res) => {
  let data = {};
  let errors = {};
  res.render("contactanos", { errors });
});

app.post("/contactanos", (req, res) => {
  let errors = {};
  let data = req.body;
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var regnumber = /^\d+$/;

  //if (req.body.inputName) {
  //res.render("enviado");
  //} else {
  //errors.name = true;
  //res.render("contactanos", { errors, data });
  //}

  if (!req.body.inputName) errors.name = true;
  if (
    !req.body.inputEmail ||
    !re.test(String(req.body.inputEmail).toLowerCase())
  )
    errors.email = true;
  if (!req.body.inputPhone || !regnumber.test(req.body.inputPhone))
    errors.phone = true;
  if (!req.body.inputMessage) errors.message = true;
  if (!req.body.inputDate) errors.date = true;

  if (
    errors.name ||
    errors.email ||
    errors.phone ||
    errors.message ||
    errors.date
  ) {
    res.render("contactanos", { errors, data });
  } else {
    res.render("enviado");
  }
});

app.get("/productos", verificarToken, (req, res) => {
  Product.find({}).then(function (documents) {
    if (documents) products = documents;
    res.render("productos", { products });
  });
});

async function verify(token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience:
      "732505487071-cufjg0cfiqb73d51ffgovdnap0cm6gv8.apps.googleusercontent.com",
  });
  const payload = ticket.getPayload();

  console.log(payload.name);
  console.log(payload.email);
  console.log(payload.picture);
  return payload;
}

app.post("/google", async (req, res) => {
  let token = req.body.idtoken;
  let googleUser = await verify(token).catch((e) => {
    return res.send("autenticacion fallida");
  });
  await User.findOne({ email: googleUser.email }, (err, usuarioDB) => {
    console.log("ACAAAAAAAAAAAAAA");
    console.log(usuarioDB);
    let token = jwt.sign(
      {
        usuario: usuarioDB,
      },
      "seed-desarrollo",
      { expiresIn: 60 * 60 * 34 * 30 }
    );
    res.cookie("token", token);
    res.redirect("/");
  });
});

app.get("/logout", (req, res) => {
  res.clearCookie("token");
  res.redirect("/");
});
app.listen(port, () => {
  console.log(`Escuchando peticiones en el puerto ${port}`);
});
